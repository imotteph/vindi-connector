const axios = require("axios");

exports.PRODUCTION = "https://app.vindi.com.br/api/v1"
exports.DEVELOPMENT = "https://sandbox-app.vindi.com.br/api/v1"

let apiURL = ""
let header = {}

exports.setToken = function(token){
    header = {
        headers: {
            'Content-Type': 'application/json'
        },
        auth: {
            username: token,
            password: ''
        }
    };
}

exports.setEnvironment = function(environment){
    apiURL = environment
}

exports.removePaymentProfile = async function(id){

    const response = await axios.delete(`${apiURL}/payment_profiles/${id}`,header).then(function(response){
        return response.data;
    }).catch(function(error){
        return error.response.data;
    });

    return response;

}

exports.getPaymentProfiles = async function(customerID){
    const response = await axios.get(`${apiURL}/payment_profiles?query=status=active customer_id=${customerID}`,header).then(function(response){
        return response.data;
    }).catch(function(error){
        return error.response.data;
    });

    return response;
}

exports.addPaymentProfile = async function(holder_name, card_expiration, card_number, card_cvv, payment_method_code, customer_id){

    const response = await axios.post(`${apiURL}/payment_profiles`,{
        "holder_name": holder_name,
        "card_expiration": card_expiration,
        "card_number": card_number,
        "card_cvv": card_cvv,
        "payment_method_code": payment_method_code,
        "customer_id": customer_id
    },header).then(function(response){
        return response.data;
    }).catch(function(error){
        return error.response.data;
    });

    return response;
}

exports.cancelBill = async function(id){

    const response = await axios.delete(`${apiURL}/bills/${id}`, header).then(resp =>{
        console.log(resp);
        return resp.data;
    }).catch(error => {
        console.log(error.response.data);
        return {}
    });

    return response;
}

exports.addBill = async function(customer_id, product_id, code, payment_method_code, due_at, price, parcelas){

    const response = await axios.post(`${apiURL}/bills`,
    {
        "customer_id": customer_id,
        "code": code,
        "payment_method_code": payment_method_code,
        "installments": parcelas,
        "due_at": due_at,
        "bill_items": [
          {
            "product_id": product_id,//426312,
            "amount": price
          }
        ]
    },header).then(function(response){
        return response.data;
    }).catch(function(error){
        return error.response.data;
    });

    return response;
}

exports.addCustomer = async function(name,cpf,email,matricula,street,number,complemento,cep,bairro,cidade,state,country,celular,telefone){

    let phones = Array();

    if (celular.trim() > 0){
        phones.push({
            "phone_type": "mobile",
            "number": celular
          });
    }

    if (telefone.trim() > 0){
        phones.push({
            "phone_type": "landline",
            "number": telefone
        });
    }

    const response = await axios.post(`${apiURL}/customers`,
    {
        "name": name,
        "registry_code": cpf,
        "email": email,
        "code": matricula,
        "notes": "ADICIONADO VIA MS FINANCEIRO",
        "metadata": "metadata",
        "address": {
          "street": street,
          "number": number,
          "additional_details": complemento,
          "zipcode": cep,
          "neighborhood": bairro,
          "city": cidade,
          "state": state,
          "country": country
        },
        "phones": phones
      },header).then(function(response){
        return response.data;
    }).catch(function(error){
        console.log(error.response.data);
    });

    return response;
    
}
exports.obtemAluno = async function(matricula){

    const response = await axios.get(`${apiURL}/customers?query=code:${matricula}`,header).then(function(response){
        return response.data;
    }).catch(function (error) {
        return error.response.data;
    }); 

    return response;

}

exports.getFaturaByCode = async function(external_code){

    const v_bill_info = await axios.get(`${apiURL}/bills?query=code=${external_code}`,header).then(function(response){
        return response.data;
    }).catch(() => {
        return [];
    })

    return v_bill_info;

}